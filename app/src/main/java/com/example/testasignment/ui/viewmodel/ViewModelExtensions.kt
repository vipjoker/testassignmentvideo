package com.example.testasignment.ui.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch


fun <T> ViewModel.taskLivedata(action: suspend () -> T): LiveData<T?> {//coroutine to  livedata callback
    val loadingLiveData = MutableLiveData<T?>()
    viewModelScope.launch(Dispatchers.IO) {
        val result = action()
        loadingLiveData.postValue(result)
    }
    return loadingLiveData
}