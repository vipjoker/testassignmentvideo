package com.example.testasignment.ui.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.testasignment.R

import com.example.testasignment.databinding.FragmentMainBinding
import com.example.testasignment.model.dto.VideoInfo
import com.example.testasignment.ui.adapter.PageContentAdapter
import com.example.testasignment.ui.viewmodel.MainViewModel
import dagger.hilt.android.AndroidEntryPoint


@AndroidEntryPoint
class MainFragment:Fragment(), PageContentAdapter.PageItemListener {
    val viewModel:MainViewModel by viewModels()
    lateinit var binding:FragmentMainBinding
    lateinit var adapter: PageContentAdapter
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = FragmentMainBinding.inflate(inflater,container,false)


        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupRecyclerView()
        viewModel.initPlayer(requireContext())
        binding.playerView.player = viewModel.player
        showControls(viewModel.showControlls)
        viewModel.loadPageContent().observe(viewLifecycleOwner){

            it?.let{
                viewModel.setVideoItems(it)
                if(viewModel.currentVideoItem.value == null) {
                    viewModel.playNext()
                }

                adapter.updateItems(it)
            }
        }

        viewModel.loading.observe(viewLifecycleOwner){
            binding.progressbar.visibility = if(it) View.VISIBLE else View.GONE
        }

        viewModel.currentVideoItem.observe(viewLifecycleOwner){ adapter.notifyDataSetChanged() }

        binding.playerView.setOnClickListener {
            viewModel.showControlls = !viewModel.showControlls
            showControls(viewModel.showControlls)
        }
        binding.ivPrev.setOnClickListener{viewModel.playPrev()}
        binding.ivNext.setOnClickListener{viewModel.playNext()}

    }

    private fun showControls(show:Boolean){
        val offset = resources.getDimension(R.dimen.links_height)
        if(!show){

            binding.ivNext.animate().translationX(offset).start()
            binding.ivPrev.animate().translationX(-offset).start()
            binding.rvLinks.animate().translationY(offset).start()
        }else{
            binding.ivNext.animate().translationX(0f).start()
            binding.ivPrev.animate().translationX(0f).start()
            binding.rvLinks.animate().translationY(0f).start()

        }
    }

    override fun onResume() {
        super.onResume()


        getActivity()?.getWindow()?.addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);

    }

    private fun setupRecyclerView() {

        binding.rvLinks.layoutManager = LinearLayoutManager(requireContext(), LinearLayoutManager.HORIZONTAL, false)
        adapter = PageContentAdapter(this,viewModel.currentVideoItem)
        binding.rvLinks.adapter = adapter
    }

    override fun onItemClicked(videoInfo: VideoInfo) {
        viewModel.changeVideoPath(videoInfo)

    }



}