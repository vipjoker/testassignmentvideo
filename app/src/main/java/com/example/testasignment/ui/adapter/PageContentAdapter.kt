package com.example.testasignment.ui.adapter

import android.graphics.Color
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.lifecycle.LiveData
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions.bitmapTransform
import com.example.testasignment.R
import com.example.testasignment.databinding.ItemLinkBinding
import com.example.testasignment.model.dto.VideoInfo

class PageContentAdapter (val listener:PageItemListener,val selected: LiveData<VideoInfo?>) : ListAdapter<VideoInfo, PageContentAdapter.ContentViewHolder>(DIFF_CALLBACK) {

        init {
            setHasStableIds(true)
        }

        private val items = mutableListOf<VideoInfo>()

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ContentViewHolder {
            val inflater = LayoutInflater.from(parent.context)
            val binding = ItemLinkBinding.inflate(inflater,parent,false)
            return ContentViewHolder(binding)
        }

        override fun getItemId(position: Int): Long {
            return items[position].id.hashCode().toLong()
        }

        override fun getItemCount(): Int {
            return items.size
        }

        override fun onBindViewHolder(holder: ContentViewHolder, position: Int) {

            val item = items[position]
            holder.bind(item)

        }


        fun updateItems(it: List<VideoInfo>) {
            items.clear()
            items.addAll(it)
            notifyDataSetChanged()
        }

        inner class ContentViewHolder(val binding: ItemLinkBinding) : RecyclerView.ViewHolder(binding.root) {
            fun bind(item: VideoInfo){
                itemView.setOnClickListener {
                    listener.onItemClicked(item)
                }
                Glide.with(binding.ivBackground).load(item.image).into(binding.ivBackground)



                binding.tvTitle.text = item.name
                binding.tvDescription.text = item.description
                if(item == selected.value){
                    binding.root.setBackgroundResource(R.drawable.background_selected_stroke)
                }else{
                    binding.root.setBackgroundColor(Color.TRANSPARENT)
                }
            }
        }

        companion object {
            val DIFF_CALLBACK = object : DiffUtil.ItemCallback<VideoInfo>() {

                override fun areItemsTheSame(oldItem: VideoInfo, newItem: VideoInfo): Boolean {
                    return oldItem.id == newItem.id
                }

                override fun areContentsTheSame(oldItem: VideoInfo, newItem: VideoInfo): Boolean {
                    return oldItem == newItem
                }
            }
        }

        interface PageItemListener{
            fun onItemClicked(link: VideoInfo)
        }
    }



