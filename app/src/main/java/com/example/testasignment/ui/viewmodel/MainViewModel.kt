package com.example.testasignment.ui.viewmodel

import android.content.Context
import android.net.Uri
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.testasignment.model.dto.VideoInfo
import com.example.testasignment.model.repository.MainRepository
import com.google.android.exoplayer2.MediaItem
import com.google.android.exoplayer2.Player
import com.google.android.exoplayer2.SimpleExoPlayer
import com.google.android.exoplayer2.extractor.DefaultExtractorsFactory
import com.google.android.exoplayer2.source.MediaSource
import com.google.android.exoplayer2.source.ProgressiveMediaSource
import com.google.android.exoplayer2.source.dash.DashMediaSource
import com.google.android.exoplayer2.source.hls.HlsMediaSource
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory
import com.google.android.exoplayer2.upstream.DefaultHttpDataSource
import com.google.android.exoplayer2.util.Util
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class MainViewModel @Inject constructor(val mainRepository: MainRepository) : ViewModel(), Player.Listener {
    var player: SimpleExoPlayer? = null
    var defDatasourceFactory: DefaultDataSourceFactory? = null
    var showControlls = true
    val loading = MutableLiveData<Boolean>()
    val currentVideoItem: MutableLiveData<VideoInfo?> = MutableLiveData()
    private val videoItems = mutableListOf<VideoInfo>()
    fun loadPageContent() = taskLivedata { mainRepository.getVideoList() }


    override fun onCleared() {
        super.onCleared()
        releasePlayer()
    }

    private fun releasePlayer() {
        val player1 = player ?: return
        player1.release()
        player = null
    }


    fun initPlayer(context: Context) {
        if (player == null) {
            defDatasourceFactory = DefaultDataSourceFactory(context, Util.getUserAgent(context, "testSource"))
            player = SimpleExoPlayer.Builder(context).build()
            player?.addListener(this)
            player?.playWhenReady = true

        }
    }

    fun changeVideoPath(videoInfo: VideoInfo) {
        currentVideoItem.value = videoInfo
        val factory = defDatasourceFactory ?: return
        val mediasource = createMediaSource(videoInfo.streamingURL, factory)
        player?.setMediaSource(mediasource)
        player?.prepare()
    }

    override fun onPlayerStateChanged(playWhenReady: Boolean, playbackState: Int) {

        if (playbackState == Player.STATE_BUFFERING)
            loading.postValue(true)
        else if (playbackState == Player.STATE_READY || playbackState == Player.STATE_ENDED)
            loading.postValue(false)
    }

    private fun createMediaSource(videoURL: String, factory: DefaultDataSourceFactory): MediaSource {
        val videoUri = Uri.parse(videoURL)
        val mediaItem = MediaItem.fromUri(videoUri)
        val lastPathSegment = videoUri.lastPathSegment ?: ""
        return when {
            lastPathSegment.contains("m3u8") -> createHlsDataSource(mediaItem, factory)
            lastPathSegment.contains("mpd") -> createDashMediaSource(videoURL)
            else -> createProgressiveMediaSource(mediaItem, factory)
        }
    }

    private fun createDashMediaSource(dashUri: String): MediaSource {
        val dataSourceFactory = DefaultHttpDataSource.Factory();

        val mediaSource = DashMediaSource.Factory(dataSourceFactory).createMediaSource(MediaItem.fromUri(dashUri))
        return mediaSource
    }

    private fun createHlsDataSource(mediaItem: MediaItem, factory: DefaultDataSourceFactory): MediaSource {
        return HlsMediaSource.Factory(factory).createMediaSource(mediaItem)
    }

    private fun createProgressiveMediaSource(mediaItem: MediaItem, factory: DefaultDataSourceFactory): MediaSource {
        return ProgressiveMediaSource.Factory(factory, DefaultExtractorsFactory())
            .createMediaSource(mediaItem)
    }


    fun setVideoItems(items: List<VideoInfo>) {
        videoItems.clear()
        videoItems.addAll(items)
    }

    fun playNext() {
        if (currentVideoItem.value == null) {
            changeVideoPath(videoItems[0])
        } else {
            val index = videoItems.indexOf(currentVideoItem.value)
            if (index + 1 < videoItems.count()) {
                changeVideoPath(videoItems[index + 1])
            }
        }
    }

    fun playPrev() {

        if (currentVideoItem.value == null) {
            changeVideoPath(videoItems[0])
        } else {
            val index = videoItems.indexOf(currentVideoItem.value)
            if (index > 0) {
                changeVideoPath(videoItems[index - 1])
            }
        }

    }
}