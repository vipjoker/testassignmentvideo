package com.example.testasignment.di.module

import com.example.testasignment.model.api.ApiService
import com.example.testasignment.model.remote.DefaultMainRepositoryRemote
import com.example.testasignment.model.remote.MainRepositoryRemote
import com.example.testasignment.model.repository.DefaultMainRepository
import com.example.testasignment.model.repository.MainRepository
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton


@Module
@InstallIn(SingletonComponent::class)
class RepositoryModule {

    @Provides
    @Singleton
    fun provideRemoteRepository(apiService: ApiService):MainRepositoryRemote {
        return DefaultMainRepositoryRemote(apiService)
    }

    @Provides
    @Singleton
    fun provideRepository(remoteRepository: MainRepositoryRemote):MainRepository {
        return DefaultMainRepository(remoteRepository)
    }


}