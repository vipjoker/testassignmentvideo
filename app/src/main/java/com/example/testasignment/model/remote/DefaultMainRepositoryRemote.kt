package com.example.testasignment.model.remote

import com.example.testasignment.C
import com.example.testasignment.model.api.ApiService
import com.example.testasignment.model.dto.VideoInfo

class DefaultMainRepositoryRemote(private val service: ApiService) :MainRepositoryRemote{

    override suspend fun getVideoInfoList(): List<VideoInfo> {
        return service.getContent(C.CONTENT_URL)
    }
}