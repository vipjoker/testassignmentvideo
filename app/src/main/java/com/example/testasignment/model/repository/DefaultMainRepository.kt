package com.example.testasignment.model.repository

import com.example.testasignment.model.dto.VideoInfo
import com.example.testasignment.model.remote.MainRepositoryRemote

class DefaultMainRepository(private val remoteRepository: MainRepositoryRemote) : MainRepository {

    override suspend fun getVideoList():List<VideoInfo>{

        val links =  remoteRepository.getVideoInfoList()
        return links

    }

}