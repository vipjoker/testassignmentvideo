package com.example.testasignment.model.remote

import com.example.testasignment.model.dto.VideoInfo


interface MainRepositoryRemote {
    suspend fun getVideoInfoList():List<VideoInfo>

}