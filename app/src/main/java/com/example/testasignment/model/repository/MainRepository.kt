package com.example.testasignment.model.repository

import com.example.testasignment.model.dto.VideoInfo

interface MainRepository {

    suspend fun getVideoList():List<VideoInfo>

}