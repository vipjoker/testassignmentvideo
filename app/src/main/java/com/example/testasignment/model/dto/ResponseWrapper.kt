package com.example.testasignment.model.dto

data class ResponseWrapper<T> (val data: T?,val error:Error?)
data class Error(val message:String)

