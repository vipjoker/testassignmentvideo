package com.example.testasignment.model.api

import com.example.testasignment.model.dto.VideoInfo
import retrofit2.http.GET
import retrofit2.http.Url

interface ApiService {

    @GET
    suspend fun  getContent(@Url url:String): List<VideoInfo>


}