package com.example.testasignment.model.dto

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "link")
data class VideoInfo (
    @PrimaryKey
    val id: Int,
    val name: String,
    val description: String,
    val image: String,
    val streamingURL: String,
    val streamingType: String,
    val drmType: String
)
